# mathexpression #

MathExpression é uma biblioteca java usada para para transformar Math Markup Language (MML) para Office Math Markup Language (OMML) e vice-versa.

## Sumário ##

* Configuração
* Como utilizar

### Configuração ###
Este é um projeto maven, portanto, para que a biblioteca funcione deve ser apenas executado o comando `mvn clean install`.


### Utilização ###

Para gerar o omml, deve ser passada uma string contendo o código mml, como mostra o código abaixo:
```java
final String omml = Expression.getInstance().mml2omml(MML);
final WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
final MainDocumentPart document = wordMLPackage.getMainDocumentPart();
```
No código acima, ```MML``` é uma String que representa o math markup language à ser convertido.

Em ```Expression.getInstance().mml2omml(MML)``` o código é convertido para omml.

Após a conversão, pode ser gerado um docx com o omml gerado executando o seguinte código:
```java
Expression.getInstance().addOmmlToDocx(omml, document);
final ByteArrayOutputStream baos = new ByteArrayOutputStream();
wordMLPackage.save(baos, Docx4J.FLAG_SAVE_FLAT_XML);
```
A chamada ```Expression.getInstance().addOmmlToDocx(omml, document);``` insere o conteúdo alinhado por padrão à esquerda, dentro da tag ```OMath``` e adiciona ao documento desejado.

Se desejar que o conteúdo esteja alinhado à direita, chame ```Expression.getInstance().addOmmlToDocx(omml, document, STJc.RIGHT);```,onde STJc é ```org.docx4j.math.STJc```